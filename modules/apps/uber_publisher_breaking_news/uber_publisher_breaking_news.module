<?php
/**
 * @file
 * Code for the Breaking News feature.
 */

include_once 'uber_publisher_breaking_news.features.inc';

/**
 * provides server information to the apps
 *
 * this hook is only called on the current profile
 * RETURN: an array of assoctive server arrays
 */
function uber_publisher_breaking_news_apps_servers_info() {
  return array(
    'uber_publisher' => array(
      'title' => t('Breaking News'), //the title to be use for the server
      'description' => t('Provide Breaking News Functionality'),
      'manifest' => 'http://app_server.local/app/query', // the location of the  json manifest
    ),
  );
}

/**
 * Implements hook_menu().
 */
function uber_publisher_breaking_news_menu() {
  $items['ajax/uber_publisher_breaking_news'] = array(
    'page callback' => '_breaking_news_ajax_call_back',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uber_publisher_breaking_news_init() {
  global $theme;

  // Dont add the javascript to admin pages
  if (path_is_admin(current_path()) || $theme === variable_get('admin_theme', 'uber_admin')) {
    return;
  }

  $js_options = array(
    'type' => 'file',
    'scope' => 'footer',
    'group' => JS_DEFAULT,
  );
  $css_options = array(
    'type' => 'file',
    'group' => CSS_DEFAULT,
  );
  $library_path = libraries_get_path('noty');

  drupal_add_library('system', 'jquery.cookie');
  drupal_add_js($library_path . '/js/noty/jquery.noty.js', $js_options);
  drupal_add_js($library_path . '/js/noty/layouts/bottomRight.js', $js_options);
  drupal_add_js(drupal_get_path('module', 'uber_publisher_breaking_news') . '/js/breaking_news_popup.js', $js_options);
  drupal_add_css(drupal_get_path('module', 'uber_publisher_breaking_news') . '/css/breaking_news_popup.css', $css_options);
}

/**
 * ajax call back to return latest Breaking news
 *
 */
function _breaking_news_ajax_call_back() {
  global $language, $user;

  $output = array();

  // @todo should be a configurable value
  $timeout = 30; // Check every 30 second

  $cache_life = time() + $timeout;

  $cached = cache_get('breaking_news', 'cache');
  if ($cached && !empty($cached->data) && $cached->expire >= time()) {
    $output = $cached->data;
  } else {
    // Make sure that we are rendering this page as an anonymous user.
    $current_user = $user;
    $user = drupal_anonymous_user();

    // get news items created 5 min ago
    $created_ago = time() - 300;

    $query = db_select('node', 'n')
                ->fields('n', array('nid', 'vid'))
                ->condition('status', 1, '=')
                ->condition('type', 'breaking_news', '=')
                ->condition('language', $language->language, '=')
                ->condition('created', $created_ago, '>=')
                ->orderBy('created', 'ASC');

    // Build result array
    if ($result = $query->execute()->fetchAllKeyed()) {
      $nids = array_keys($result);
      $nodes = node_load_multiple($nids);
      $build = node_view_multiple($nodes, 'teaser', 0, $language->language);
      foreach($build['nodes'] as $nid => &$node) {
        if (is_numeric($nid)) {
          $output['nodes']["{$nid}"] = array();
          $output['nodes']["{$nid}"]['html'] .= render($node);
       }
      }
      $output['cache_life'] = $cache_life;
    }

    // Return the user from anonymous user to the current user
    $user = $current_user;

    cache_set('breaking_news', $output, 'cache', $cache_life);
  }

  // Add cache headers equal to the time we are saving the results for.
  drupal_add_http_header('Cache-Control', 'public, max-age=' . $timeout);

  drupal_json_output($output);
}

/**
 * Implements hook_libraries_info().
 *
 */
function uber_publisher_breaking_news_libraries_info() {
  $libraries = array();

  $libraries['noty'] = array(
    'name' => 'Bootstrap Notify',
    'vendor url' => 'https://github.com/goodybag/bootstrap-notify/',
    'download url' => 'http://github.com/goodybag/bootstrap-notify/archive/v0.2.0.tar.gz',
    'version arguments' => array(
      'file' => 'js/noty/jquery.noty.js',
      'pattern' => '/@version version: 2.3.2/',
      'lines' => 3,
    ),
    'version callback' => '_bootstrap_notify_version',
    'files' => array(
      'js' => array(
        'js/noty/jquery.noty.js',
      ),
    )
  );

  return $libraries;
}

function _bootstrap_notify_version() {
  // This is to prevent libraries from actually checking the vesion
  return TRUE;
}

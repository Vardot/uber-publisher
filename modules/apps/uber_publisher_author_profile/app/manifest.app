name = Author Profile
description = "Uber Publisher Author Profile add a profile for you content."
machine_name = uber_publisher_author_profile
version = 7.x-1.0
author = Vardot
author_url = http://www.vardot.com/
logo = logo.png
screenshots[] = screenshot.jpg
downloadable = uber_publisher_author_profile 1.0

dependencies[uber_publisher_core] = uber_publisher_core 1.0

downloadables[uber_publisher_breaking_news 7.x-1.0] = http://app_server.local/sites/default/files/uber_publisher_author_profile-7.x-1.0.tar.gz

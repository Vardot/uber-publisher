<?php
/**
 * @file
 * uber_publisher_author_profile.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uber_publisher_author_profile_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uber_publisher_article-field_author'
  $field_instances['node-uber_publisher_article-field_author'] = array(
    'bundle' => 'uber_publisher_article',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 5,
      ),
      'full' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'title_only' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'vertical_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'entityconnect_unload_add' => 0,
    'entityconnect_unload_edit' => 0,
    'field_name' => 'field_author',
    'label' => 'Author',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-uber_publisher_author-body'
  $field_instances['node-uber_publisher_author-body'] = array(
    'bundle' => 'uber_publisher_author',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'mini_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'smart_trim',
        'settings' => array(
          'more_link' => 1,
          'more_text' => 'Views profile',
          'summary_handler' => 'trim',
          'trim_length' => 400,
          'trim_options' => array(
            'text' => 'text',
          ),
          'trim_suffix' => '...',
          'trim_type' => 'chars',
        ),
        'type' => 'smart_trim_format',
        'weight' => 2,
      ),
      'title_only' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'vertical_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Biography',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'basic_editor' => 'basic_editor',
          'html_editor' => 'html_editor',
          'plain_text' => 'plain_text',
          'visual_editor' => 'visual_editor',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 1,
        'default_order_wrapper' => array(
          'formats' => array(
            'basic_editor' => array(
              'weight' => -49,
            ),
            'html_editor' => array(
              'weight' => -47,
            ),
            'plain_text' => array(
              'weight' => -50,
            ),
            'visual_editor' => array(
              'weight' => -48,
            ),
          ),
        ),
      ),
      'display_summary' => 1,
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => '',
        'maxlength_js_enforce' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_label_summary' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_summary' => '',
        'maxlength_js_truncate_html' => 0,
        'rows' => 8,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-uber_publisher_author-field_real_user'
  $field_instances['node-uber_publisher_author-field_real_user'] = array(
    'bundle' => 'uber_publisher_author',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 12,
      ),
      'mini_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 11,
      ),
      'title_only' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'vertical_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_real_user',
    'label' => 'Real user',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete_tags',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-uber_publisher_author-field_social_links'
  $field_instances['node-uber_publisher_author-field_social_links'] = array(
    'bundle' => 'uber_publisher_author',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'socialfield',
        'settings' => array(
          'link_text' => FALSE,
        ),
        'type' => 'socialfield_formatter',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'socialfield',
        'settings' => array(
          'link_text' => 1,
        ),
        'type' => 'socialfield_formatter',
        'weight' => 3,
      ),
      'mini_teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'title_only' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'vertical_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_social_links',
    'label' => 'Social links',
    'required' => 0,
    'settings' => array(
      'services' => array(
        'facebook' => 'facebook',
        'twitter' => 'twitter',
      ),
      'used_services' => array(
        'delicious' => 'delicious',
        'digg' => 'digg',
        'facebook' => 'facebook',
        'flickr' => 'flickr',
        'googleplus' => 'googleplus',
        'linkedin' => 'linkedin',
        'pinterest' => 'pinterest',
        'reddit' => 'reddit',
        'slideshare' => 'slideshare',
        'twitter' => 'twitter',
        'vimeo' => 'vimeo',
        'yahoo' => 'yahoo',
        'youtube' => 'youtube',
      ),
      'user_register_form' => FALSE,
      'weights' => array(
        'delicious' => 0,
        'digg' => 1,
        'facebook' => -10,
        'flickr' => -1,
        'googleplus' => -8,
        'linkedin' => -7,
        'myspace' => 4,
        'pinterest' => -5,
        'reddit' => -3,
        'slideshare' => -4,
        'stumbleupon' => 2,
        'twitter' => -9,
        'vimeo' => -2,
        'yahoo' => 3,
        'youtube' => -6,
      ),
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'socialfield',
      'settings' => array(),
      'type' => 'socialfield_widget',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-uber_publisher_author-field_up_thumbnail'
  $field_instances['node-uber_publisher_author-field_up_thumbnail'] = array(
    'bundle' => 'uber_publisher_author',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'media_thumbnail',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'mini_teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'article_mini_teaser',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'article_horizontal_teaser',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'title_only' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'vertical_teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => 'article_vertical_teaser',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_up_thumbnail',
    'label' => 'Profile picture',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'epsacrop' => array(
        'styles' => array(
          'article_horizontal_teaser' => 0,
          'article_mini_teaser' => 0,
          'article_thumbnail' => 0,
          'article_vertical_teaser' => 0,
          'flexslider_default' => 0,
          'views_classic_slider' => 0,
        ),
      ),
      'file_directory' => '',
      'file_extensions' => 'png jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'transliterate' => 1,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'transliterate' => 0,
          ),
          'value' => 'profile_picture/',
        ),
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '4M',
      'max_resolution' => '',
      'min_resolution' => '100x100',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'upload' => 0,
        ),
      ),
      'type' => 'media_generic',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Author');
  t('Biography');
  t('Profile picture');
  t('Real user');
  t('Social links');

  return $field_instances;
}

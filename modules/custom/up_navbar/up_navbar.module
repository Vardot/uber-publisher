<?php

/**
 * @file
 * Administration toolbar for quick access to top level administration items.
 */

/**
 * Implements hook_permission().
 */
function up_navbar_permission() {
  return array(
    'access up navbar' => array(
      'title' => t('Use the administration navbar'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function up_navbar_menu() {
  $system_module_path = drupal_get_path('module', 'system');

  // Add a new admin menu item as a stub to group menu items in an admin page
  $items['admin/home'] = array(
    'title' => 'Admin Home',
    'description' => 'Administer Home.',
    'access arguments' => array('administer themes'),
    'page callback' => 'system_admin_menu_block_page',
    'weight' => 9,
    'menu_name' => 'management',
    'file' => '/system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function up_navbar_theme($existing, $type, $theme, $path) {
  $module_path = drupal_get_path('module', 'up_navbar');

  $items = array();
  $items['up_navbar_header'] = array(
    'render element' => 'up_navbar_header',
    'template' => 'up-navbar-header',
    'file' => '/theme.inc',
    'path' => $module_path . '/theme',
  );
  $items['up_navbar_sidebar'] = array(
    'render element' => 'up_navbar_sidebar',
    'template' => 'up-navbar-sidebar',
    'file' => '/theme.inc',
    'path' => $module_path . '/theme',
  );
  $items['up_navbar_face_box'] = array(
    'render element' => 'up_navbar_face_box',
    'template' => 'up-navbar-face-box',
    'file' => '/theme.inc',
    'path' => $module_path . '/theme',
  );
  $items['up_navbar_link'] = array(
    'render element' => 'element',
    'file' => '/theme.inc',
    'path' => $module_path . '/theme',
  );

  return $items;
}

/**
 * Implements hook_preprocess_html().
 */
function up_navbar_preprocess_html(&$vars) {
  if (isset($vars['page']['page_bottom']['up_navbar_header']) && user_access('access up navbar')) {
    $vars['classes_array'][] = 'up-navbar-top';
  }
  if (isset($vars['page']['page_bottom']['up_navbar_sidebar']) && user_access('access up navbar')) {
    $vars['classes_array'][] = 'up-navbar-sidebar';
  }
}

/**
 * Implements hook_css_alter().
 */
function up_navbar_js_alter(&$javascript) {
  // Replace escape_admin js (escapeAdmin.js) with our version of js file
  if (isset($javascript[drupal_get_path('module', 'escape_admin') . '/js/escapeAdmin.js'])) {
    $module_path = drupal_get_path('module', 'up_navbar');
    unset($javascript[drupal_get_path('module', 'escape_admin') . '/js/escapeAdmin.js']);
    drupal_add_js($module_path . '/js/escapeAdmin.js', $options = NULL);
  }
}

/**
 * Implements hook_page_build().
 */
function up_navbar_page_build(&$page) {
  $page['page_bottom']['up_navbar_header'] = array(
    '#pre_render' => array('_up_navbar_header_pre_render'),
    '#access' => user_access('access up navbar'),
    '#weight' => 100,
  );
  $page['page_bottom']['up_navbar_sidebar'] = array(
    '#pre_render' => array('_up_navbar_sidebar_pre_render'),
    '#access' => user_access('access up navbar'),
    '#weight' => 101,
  );
}

/**
 * Prerender function for the toolbar.
 */
function _up_navbar_header_pre_render($build) {
  $module_path = drupal_get_path('module', 'up_navbar');
  $build = array(
    '#theme' => 'up_navbar_header',
    '#attached'=> array(
      'js' => array(
        $module_path . '/js/up_navbar.js',
      ),
      'css' => array(
        $module_path . '/css/up_navbar.css',
      ),
    ),
  );

  $build['header_menu'] = array(
    '#pre_render' => array('_up_navbar_header_menu_pre_render'),
  );


  $header_menu_tree = menu_tree_all_data('add-content', NULL, 2);
  if (function_exists('i18n_menu_localize_tree')) {
    $header_menu_tree = i18n_menu_localize_tree($header_menu_tree);
  }
  $build['header_menu'] = _up_navbar_menu_tree_output($header_menu_tree);

  $build['user_box'] = array(
    '#pre_render' => array('_up_navbar_user_box_pre_render'),
  );

  return $build;
}

/**
 * Prerender function for the toolbar.
 */
function _up_navbar_sidebar_pre_render($build) {
  $module_path = drupal_get_path('module', 'up_navbar');
  $build = array(
    '#theme' => 'up_navbar_sidebar',
    '#attached'=> array(
      'js' => array(
        $module_path . '/js/up_navbar.js',
      ),
      'css' => array(
        $module_path . '/css/up_navbar.css',
      ),
    ),
  );

  // Add home link as a fall back
  $build['home_link'] = array(
    '#theme' => 'link',
    '#path' => '<front>',
    '#text' => t('Home'),
    '#options' => array(
      'html' => TRUE,
      'attributes' => array(
        'title' => t('Home'),
        'class' => array('toolbar-icon-home'),
      ),
    ),
  );

  // Add support for Escape admin module
  if (module_exists('escape_admin')) {
    $build['home_link']['#text'] = t('Back to site');
    $build['home_link']['#options']['attributes']['title'] = t('Return to site content');
    $build['home_link']['#options']['attributes']['class'][] = 'toolbar-icon-escape-admin';
    $build['home_link']['#options']['attributes']['data-toolbar-escape-admin'] = NULL;
    $build['#attached']['library'][] = array('escape_admin', 'toolbar');
  }
  $build['home_link']['#text'] = '<i class="up-icon"></i><span class="title">' . $build['home_link']['#text'] . '</span>';

  // Add shortcut menu
  if (module_exists('shortcut')) {
    $shortcut_set = shortcut_current_displayed_set();
    $shortcut_tree = menu_tree_all_data($shortcut_set->set_name, NULL, 1);
    if (function_exists('i18n_menu_localize_tree')) {
      $shortcut_tree = i18n_menu_localize_tree($shortcut_tree);
    }
    $build['shortcut_tree'] = _up_navbar_menu_tree_output($shortcut_tree);
  }

  // Add management menu
  $management_tree = array();
  $management_tree_row = menu_tree_all_data('management', NULL, 3);
  foreach ($management_tree_row as &$tree) {
    $management_tree = $tree['below'];
  }
  if (function_exists('i18n_menu_localize_tree')) {
    $tree = i18n_menu_localize_tree($management_tree);
  }
  $build['management_menu'] = _up_navbar_menu_tree_output($management_tree);

  return $build;
}

/**
 * Prerender function for the user avatar and menu.
 */
function _up_navbar_user_box_pre_render($build) {
  $user = user_load($GLOBALS['user']->uid);
  $module_path = drupal_get_path('module', 'up_navbar');

  $build = array(
    '#theme' => 'up_navbar_face_box',
  );

  $avatar_style = 'icon';
  if (!empty($user->picture)) {
    $filepath = $user->picture->uri;
  }
  else {
    if (variable_get('user_picture_default', '')) {
      $filepath = variable_get('user_picture_default', '');
    } else {
      $filepath = drupal_get_path('module', 'up_navbar') . '/images/default.png';
    }

    $style_filepath = image_style_path('icon', $filepath);
    if (!file_exists($style_filepath)) {
      image_style_create_derivative($avatar_style, $filepath, $style_filepath);
    }
  }

  $alt = t("@user's picture", array('@user' => format_username($user)));
  $build['user_avatar'] = theme('image_style', array(
    'style_name' => $avatar_style,
    'path' => $filepath,
    'alt' => $alt,
    'title' => $alt
  ));

  $build['user_menu'] = array(
    '#theme' => 'links',
    '#links' => menu_navigation_links('user-menu'),
    '#attributes' => array(
      'class' => array('user-menu'),
    ),
  );

  return $build;
}

/**
 * Our version of menu_tree_output, see Drupal core menu_tree_output().
 */
function _up_navbar_menu_tree_output(&$tree) {
  $build = array();
  $items = array();

  foreach ($tree as $data) {
    if ($data['link']['access'] && !$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  $router_item = menu_get_item();
  $num_items = count($items);
  foreach ($items as $i => $data) {
    $class = array();
    if ($i == 0) {
      $class[] = 'first';
    }
    if ($i == $num_items - 1) {
      $class[] = 'last';
    }
    if ($data['link']['has_children'] && $data['below']) {
      $class[] = 'expanded';
    }
    elseif ($data['link']['has_children']) {
      $class[] = 'collapsed';
    }
    else {
      $class[] = 'leaf';
    }
    if ($data['link']['in_active_trail']) {
      $class[] = 'active-trail';
      $data['link']['localized_options']['attributes']['class'][] = 'active-trail';
    }
    if ($data['link']['href'] == $router_item['tab_root_href'] && $data['link']['href'] != $_GET['q']) {
      $class[] = 'active-leaf';
      $data['link']['localized_options']['attributes']['class'][] = 'active';
    }
    $class[] = 'up-navbar-name-' . strtolower(str_replace(' ', '-', $data['link']['link_title']));
    $class[] = 'link-depth-' . $data['link']['depth'];

    // Allow menu-specific theme overrides.
    $element['#theme'] = 'up_navbar_link';
    $element['#attributes']['class'] = $class;
    $element['#title'] = $data['link']['title'];
    $element['#href'] = $data['link']['href'];
    $element['#localized_options'] = !empty($data['link']['localized_options']) ? $data['link']['localized_options'] : array();
    $element['#below'] = $data['below'] ? _up_navbar_menu_tree_output($data['below']) : $data['below'];
    $element['#original_link'] = $data['link'];

    // Index using the link's unique mlid.
    $build[$data['link']['mlid']] = $element;
  }
  if ($build) {
    // Make sure drupal_render() does not re-order the links.
    $build['#sorted'] = TRUE;
    // Add the theme wrapper for outer markup.
    // Allow menu-specific theme overrides.
    $build['#theme_wrappers'][] = 'varbase_neutral_tree';
  }

  return $build;
}
